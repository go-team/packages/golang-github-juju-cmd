From: Mathias Gibbens <mathias@calenhad.com>
Date: Thu, 27 Jul 2023 16:02:48 +0800
Subject: Remove use of the juju/utils library (break bi-directional
 dependency loop)

Forwarded: https://github.com/juju/cmd/issues/78
---
 cmd.go          |  3 +--
 cmd_utils.go    | 62 +++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 filevar.go      |  6 ++----
 filevar_test.go |  5 ++---
 4 files changed, 67 insertions(+), 9 deletions(-)
 create mode 100644 cmd_utils.go

diff --git a/cmd.go b/cmd.go
index 6bba560..369ca4a 100644
--- a/cmd.go
+++ b/cmd.go
@@ -18,7 +18,6 @@ import (
 	"github.com/juju/ansiterm"
 	"github.com/juju/gnuflag"
 	"github.com/juju/loggo"
-	"github.com/juju/utils/v3"
 )
 
 // RcPassthroughError indicates that a Juju plugin command exited with a
@@ -236,7 +235,7 @@ func (ctx *Context) Setenv(key, value string) error {
 // interpreted as relative to ctx.Dir and with "~/" replaced with users
 // home dir.
 func (ctx *Context) AbsPath(path string) string {
-	if normalizedPath, err := utils.NormalizePath(path); err == nil {
+	if normalizedPath, err := normalizePath(path); err == nil {
 		path = normalizedPath
 	}
 	if filepath.IsAbs(path) {
diff --git a/cmd_utils.go b/cmd_utils.go
new file mode 100644
index 0000000..a94863b
--- /dev/null
+++ b/cmd_utils.go
@@ -0,0 +1,62 @@
+// Copyright 2013 Canonical Ltd.
+// Licensed under the LGPLv3, see LICENCE file for details.
+// Methods are copied from juju/utils to break a dependency loop
+// See https://github.com/juju/cmd/issues/78
+
+package cmd
+
+import (
+	"fmt"
+	"os"
+	"os/user"
+	"path/filepath"
+	"regexp"
+
+	"github.com/juju/errors"
+)
+
+func homeDir(userName string) (string, error) {
+	u, err := user.Lookup(userName)
+	if err != nil {
+		return "", errors.NewUserNotFound(err, "no such user")
+	}
+	return u.HomeDir, nil
+}
+
+// userHomeDir returns the home directory for the specified user, or the
+// home directory for the current user if the specified user is empty.
+func userHomeDir(userName string) (hDir string, err error) {
+	if userName == "" {
+		// TODO (wallyworld) - fix tests on Windows
+		// Ordinarily, we'd always use user.Current() to get the current user
+		// and then get the HomeDir from that. But our tests rely on poking
+		// a value into $HOME in order to override the normal home dir for the
+		// current user. So we're forced to use Home() to make the tests pass.
+		// All of our tests currently construct paths with the default user in
+		// mind eg "~/foo".
+		return os.Getenv("HOME"), nil
+	}
+	hDir, err = homeDir(userName)
+	if err != nil {
+		return "", err
+	}
+	return hDir, nil
+}
+
+// Only match paths starting with ~ (~user/test, ~/test). This will prevent
+// accidental expansion on Windows when short form paths are present (C:\users\ADMINI~1\test)
+var userHomePathRegexp = regexp.MustCompile("(^~(?P<user>[^/]*))(?P<path>.*)")
+
+// normalizePath expands a path containing ~ to its absolute form,
+// and removes any .. or . path elements.
+func normalizePath(dir string) (string, error) {
+	if userHomePathRegexp.MatchString(dir) {
+		user := userHomePathRegexp.ReplaceAllString(dir, "$user")
+		userHomeDirVar, err := userHomeDir(user)
+		if err != nil {
+			return "", err
+		}
+		dir = userHomePathRegexp.ReplaceAllString(dir, fmt.Sprintf("%s$path", userHomeDirVar))
+	}
+	return filepath.Clean(dir), nil
+}
diff --git a/filevar.go b/filevar.go
index 497f7fe..6ea9505 100644
--- a/filevar.go
+++ b/filevar.go
@@ -8,8 +8,6 @@ import (
 	"io"
 	"io/ioutil"
 	"os"
-
-	"github.com/juju/utils/v3"
 )
 
 // FileVar represents a path to a file.
@@ -58,7 +56,7 @@ func (f *FileVar) Open(ctx *Context) (io.ReadCloser, error) {
 		return ioutil.NopCloser(ctx.Stdin), nil
 	}
 
-	path, err := utils.NormalizePath(f.Path)
+	path, err := normalizePath(f.Path)
 	if err != nil {
 		return nil, err
 	}
@@ -74,7 +72,7 @@ func (f *FileVar) Read(ctx *Context) ([]byte, error) {
 		return ioutil.ReadAll(ctx.Stdin)
 	}
 
-	path, err := utils.NormalizePath(f.Path)
+	path, err := normalizePath(f.Path)
 	if err != nil {
 		return nil, err
 	}
diff --git a/filevar_test.go b/filevar_test.go
index 7d48c68..4816ce1 100644
--- a/filevar_test.go
+++ b/filevar_test.go
@@ -13,7 +13,6 @@ import (
 	"github.com/juju/gnuflag"
 	gitjujutesting "github.com/juju/testing"
 	jc "github.com/juju/testing/checkers"
-	"github.com/juju/utils/v3"
 	gc "gopkg.in/check.v1"
 
 	"github.com/juju/cmd/v3"
@@ -93,7 +92,7 @@ func (FileVarSuite) checkOpen(c *gc.C, file io.ReadCloser, expected string) {
 }
 
 func (s *FileVarSuite) TestOpenTilde(c *gc.C) {
-	path := filepath.Join(utils.Home(), "config.yaml")
+	path := filepath.Join(os.Getenv("HOME"), "config.yaml")
 	err := ioutil.WriteFile(path, []byte("abc"), 0644)
 	c.Assert(err, gc.IsNil)
 
@@ -142,7 +141,7 @@ func (s *FileVarSuite) TestOpenInvalid(c *gc.C) {
 }
 
 func (s *FileVarSuite) TestReadTilde(c *gc.C) {
-	path := filepath.Join(utils.Home(), "config.yaml")
+	path := filepath.Join(os.Getenv("HOME"), "config.yaml")
 	err := ioutil.WriteFile(path, []byte("abc"), 0644)
 	c.Assert(err, gc.IsNil)
 
